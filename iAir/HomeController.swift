//
//  ViewController.swift
//  iAir
//
//  Created by Service Informatique on 27/02/2020.
//  Copyright © 2020 aag. All rights reserved.
//

import UIKit

class MeasurementsLabel {
    var text: String
    var color: UIColor
    
    init(text: String, color: UIColor) {
        self.text = text
        self.color = color
    }
}

class MeasureLine {
    var indice: String
    var measure: String
    var measureAVG: String
    var category: String
    
    init(indice: String, measure: String, measureAVG: String, category: String) {
        self.indice = indice
        self.measure = measure
        self.measureAVG = measureAVG
        self.category = category
    }
}

class HomeController: UIViewController {
    
    @IBOutlet weak var atmoLabel: UILabel!
    @IBOutlet weak var labelsSV: UIStackView!
    @IBOutlet weak var measurementsSV: UIStackView!
    
    var items: [MeasurementsLabel] = [
        MeasurementsLabel(text: "Bonne qualité d'air", color: UIColor.systemGreen),
        MeasurementsLabel(text: "Port du masque requis", color: UIColor.systemYellow),
        MeasurementsLabel(text: "Fort taux de particules fines", color: UIColor.systemRed)
    ]
    
    var measurements: [MeasureLine] = [
        MeasureLine(indice: "dead", measure: "37 ug/m3", measureAVG: "22 ug/m3", category: "SO2"),
        MeasureLine(indice: "happy", measure: "17 ug/m3", measureAVG: "20 ug/m3", category: "C3"),
        MeasureLine(indice: "straigh", measure: "16 ug/m3", measureAVG: "15 ug/m3", category: "C4"),
    ]
    
    
    override func viewDidLoad() {
        self.fillAtmoLabel(atmo: 2, color: UIColor.systemGreen)
        self.fillLabelsSV(mLabels: items)
        self.fillMeasurementsSV(measurements: measurements)
    }
    
    /// Styling and filling atmo user interface
    func fillAtmoLabel(atmo: Int, color: UIColor) {
        self.atmoLabel.text = String(atmo)
        self.atmoLabel.layer.borderColor = color.cgColor
        self.atmoLabel.layer.borderWidth = 3.0
        self.atmoLabel.layer.masksToBounds = true
        self.atmoLabel.layer.cornerRadius = self.atmoLabel.frame.width/2
    }
    
    /// Styling and filling measurement user interface
    func fillLabelsSV(mLabels: [MeasurementsLabel]) {
        for mLabel in mLabels {
            let label: UILabel = UILabel()
            label.text = mLabel.text
            label.backgroundColor = mLabel.color
            label.textAlignment = NSTextAlignment.center
            self.labelsSV.addArrangedSubview(label)
        }
    }
    
    /// Styling and filling labels user interface
    func fillMeasurementsSV(measurements: [MeasureLine]) {
        
        var color: UIColor = UIColor.black
        for measure in measurements {
            
            switch measure.indice {
            case "happy":
                color = UIColor.systemGreen
            case "dead":
                color = UIColor.systemRed
            case "straigh":
                color = UIColor.systemYellow
            default:
                color = UIColor.systemBlue
            }
            
            let image: UIImage = UIImage(named: measure.indice)!
            let imageView: UIImageView = UIImageView(image: image)
            imageView.frame = CGRect(x: 0, y: 0, width: 42, height: 42)
            
            let measureLabel: UILabel = UILabel()
            measureLabel.textColor = color
            measureLabel.text = measure.measure
            
            let expectedMeasureLabel: UILabel = UILabel()
            expectedMeasureLabel.text = measure.measureAVG
            
            let categoryLabel: UILabel = UILabel()
            categoryLabel.text = measure.category
            
            let lineSV: UIStackView = UIStackView()
            lineSV.axis = NSLayoutConstraint.Axis.horizontal
            
            
            lineSV.addArrangedSubview(imageView)
            lineSV.addArrangedSubview(measureLabel)
            lineSV.addArrangedSubview(expectedMeasureLabel)
            lineSV.addArrangedSubview(categoryLabel)
            
            self.measurementsSV.addArrangedSubview(lineSV)
        }
    }
}

