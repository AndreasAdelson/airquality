import Foundation
import SwiftyJSON

class Measurement {
    var parameter: String
    var value: Double
    var unit: String
    
    init(parameter: String, value: Double, unit: String) {
        
        self.parameter = parameter
        self.value = value
        self.unit = unit
    }
    
    static func requestWindApi() {
        var measurements: [Measurement] = []
        var cityMeasurements: CityMeasurements?
        var api: String = "https://api.openaq.org/v1/latest"
        let longitude: Double = -1.66
        let lattitude: Double = 48.10
        
        api += "?coordinates=\(String(lattitude)),\(String(longitude))"
        let url = URL(string: api)
        var request = URLRequest(url: url!)
        request.httpMethod = "get"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            do {
                let json = try JSON(data: data!)
                
                Location.convertFromJson(json["results"][0]) { (receivedLocation) in
                    for  jsonMeasurement in json["results"][0]["measurements"].arrayValue {
                        measurements.append(Measurement.convertFromJson(_: jsonMeasurement))
                    }
                    cityMeasurements = CityMeasurements(name: receivedLocation, measurements: measurements)
                }
            } catch let e {
                print(e)
            }
            
        }.resume()
    }
    
    static func convertFromJson(_ data: JSON) -> Measurement{
        
        let parameter: String = data["parameter"].stringValue
        let value: Double = data["value"].doubleValue
        let unit: String = data["unit"].stringValue
        
        
        return Measurement(parameter: parameter, value: value, unit: unit)
    }
    
}
