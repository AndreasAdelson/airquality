import Foundation
import SwiftyJSON

class Location {
    var name: String
    var country: String
    var city: String
    var long: Double
    var lat: Double
    
    init(name: String, country: String, city: String, long: Double, lat: Double) {
        self.name = name
        self.country = country
        self.city = city
        self.long = long
        self.lat = lat
    }
    
    
    static func convertFromJson(_ data: JSON, callback: @escaping (Location) -> Void) {
        
        let country: String = data["country"].stringValue
        let city: String = data["city"].stringValue
        let long: Double = data["coordinates"]["longitude"].doubleValue
        let lat: Double = data["coordinates"]["latitude"].doubleValue
        var name: String = ""
        
        let api: String = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lat),\(long)&key=AIzaSyAw6qf4ZCwvdnKdmb37CNPwtO2-4mgdAIM"
        let url = URL(string: api)
        var request = URLRequest(url: url!)
        request.httpMethod = "get"
        URLSession.shared.dataTask(with: request) {(data, response, error) in
            do {
                let json = try JSON(data: data!)
                let compound_code_arr = json["plus_code"]["compound_code"].stringValue.components(separatedBy: " ")
                name = compound_code_arr[1]
                name = String(name.dropLast())
                print(name)
                callback(Location(name: name, country: country, city: city, long: long, lat: lat))
            } catch let e {
                print(e)
            }
        }.resume();
        
    }
    
//    func getCityNameFromCoordinates(long: Double, lat: Double) -> String {
//        var name = ""
//        let api: String = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lat),\(long)&key=AIzaSyAw6qf4ZCwvdnKdmb37CNPwtO2-4mgdAIM"
//        let url = URL(string: api)
//        var request = URLRequest(url: url!)
//        request.httpMethod = "get"
//
//        URLSession.shared.dataTask(with: request) {(data, response, error) in
//            do {
//                let json = try JSON(data: data!)
//                var compound_code = json["plus_code"]["compound_code"].stringValue
//                print(compound_code)
//            } catch let e {
//                print(e)
//            }
//        }
//        return name
//    }
}
