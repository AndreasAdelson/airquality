import Foundation
import SwiftyJSON

enum Label {
    case happy, unhappy, mitigated
}

class CityMeasurements {
    var name: Location
    var measurements: [Measurement]
//    var label: Label
    
    init(name: Location, measurements: [Measurement]) {
        self.name = name
        self.measurements = measurements
//        self.label = label
    }
}
